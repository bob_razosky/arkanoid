/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arka.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 21:20:19 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/03 21:20:33 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARKA_H
# define ARKA_H

# pragma GCC diagnostic ignored "-Wdeprecated-declarations"

# include "glfw3.h"
# include <fcntl.h>
# include "libft.h"
# include <math.h>
# include <GLUT/glut.h>

# define SBX		S->step_ball_x
# define SBY		S->step_ball_y
# define STEP		0.01f
# define BALL_SIZE	0.03f
# define SIZE		9
# define S			singleton()
# define STEP		0.01f
# define BALL_SIZE	0.03f
# define MAP_POS	S->map[pos->mapy][pos->mapx]
# define MAX_LEVELS	5

typedef enum			s_game_state
{
	GAME_START,
	GAME_FAILED,
	GAME_WON,
	GAME_RUNNING,
	GAME_WON_EVERYTHING
}						t_game_state;

typedef struct			s_pos
{
	int					mapx;
	int					mapy;
	float				x1;
	float				x2;
	float				y1;
	float				y2;
	struct s_pos		*next;
}						t_pos;

typedef struct			s_arka
{
	float				x;
	float				ball_x;
	float				ball_y;
	float				step_ball_x;
	float				step_ball_y;
	int					score;
	int					map[SIZE + 1][SIZE + 1];
	int					level;
	t_game_state		game_state;
	int					life;
	GLFWwindow			*win;
	char				*player;
}						t_arka;

void					ft_player(char *player, int ac);
void					ft_score(char *scores);
int						verif_map(t_arka *arka);
void					fill_map(t_arka *arka, char *s, int i);
void					ball_collide(t_pos *pos);
void					draw_ball(float radius);
void					draw_palette(void);
void					draw_case(float x, float y, int value);
void					init(void);
t_pos					*pos_add(t_pos *next, int y, int x);
t_arka					*singleton(void);
void					test2(GLFWwindow *window);
void					test3(GLFWwindow *window, t_pos *pos);
t_pos					*test4(float y, t_pos *pos);
void					game_routine();

#endif
