/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 20:59:29 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/03 21:21:08 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arka.h"

static void		key_callback(GLFWwindow *window, int key,
	int scancode, int action, int mods)
{
	(void)scancode;
	(void)mods;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	else if ((key == GLFW_KEY_LEFT && action == GLFW_REPEAT) ||
(key == GLFW_KEY_LEFT && action == GLFW_PRESS))
		S->x = S->x - 0.2 <= -1 ? S->x : S->x - 0.1f;
	else if ((key == GLFW_KEY_RIGHT && action == GLFW_REPEAT)
		|| (key == GLFW_KEY_RIGHT && action == GLFW_PRESS))
		S->x = S->x + 0.2 >= 1 ? S->x : S->x + 0.1f;
}

void			test2(GLFWwindow *window)
{
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
}

static void		sub_test3(GLFWwindow *window)
{
	float		ratio;
	int			width;
	int			height;

	glfwGetFramebufferSize(window, &width, &height);
	ratio = width / (float)height;
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	draw_palette();
	glLoadIdentity();
	draw_ball(BALL_SIZE);
}

void			test3(GLFWwindow *window, t_pos *pos)
{
	sub_test3(window);
	ball_collide(pos);
	while (pos)
	{
		if (S->ball_x + 0.03f >= pos->x1 &&
			S->ball_x - 0.03f <= pos->x2 &&
			S->ball_y + 0.03f >= pos->y1 &&
			S->ball_y - 0.03f <= pos->y2)
		{
			if (MAP_POS > 0 && MAP_POS < 9)
			{
				S->score++;
				S->map[pos->mapy][pos->mapx] -= 1;
			}
			break ;
		}
		pos = pos->next;
	}
	if (!verif_map(S))
		S->game_state = GAME_WON;
}

t_pos			*test4(float y, t_pos *pos)
{
	float			x;

	while (y < 10)
	{
		x = 0;
		while (x < 10)
		{
			if (S->map[(int)y][(int)x] > 0)
			{
				pos = pos_add(pos, y, x);
				draw_case(0.9 - (0.2 * x), 0.9 - (0.2 * y),
S->map[(int)y][(int)x]);
			}
			x++;
		}
		y++;
	}
	return (pos);
}
