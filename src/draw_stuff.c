/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_stuff.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 21:19:51 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/03 21:19:54 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arka.h"

void			sub_collide(t_pos *pos)
{
	while (pos)
	{
		if (S->ball_x + 0.03f >= pos->x1 && S->ball_x - 0.03f <= pos->x2
			&& S->ball_y + 0.03f >= pos->y1 && S->ball_y - 0.03f <= pos->y2)
		{
			if (S->ball_x + 0.02f - SBX <= pos->x1)
				SBX = SBX < 0 ? SBX : -SBX;
			else if (S->ball_x + 0.02f - SBX >= pos->x2)
				SBX = SBX >= 0 ? SBX : -SBX;
			if (S->ball_y + 0.02f <= pos->y1)
				SBY = SBY < 0 ? SBY : -SBY;
			else if (S->ball_y + 0.02f >= pos->y2)
				SBY = SBY >= 0 ? SBY : -SBY;
			break ;
		}
		pos = pos->next;
	}
}

void			ball_collide(t_pos *pos)
{
	S->ball_y += SBY;
	S->ball_x += SBX;
	if (S->ball_x >= 1.f - BALL_SIZE)
		SBX = SBX < 0 ? SBX : -SBX;
	if (S->ball_y >= 1.f - BALL_SIZE)
		SBY = -STEP;
	if (S->ball_x <= -1.f + BALL_SIZE)
		SBX = SBX > 0 ? SBX : -SBX;
	sub_collide(pos);
	if (S->ball_y <= (-1.f + 0.2 + BALL_SIZE))
	{
		if (S->ball_x >= S->x && S->ball_x <= (S->x + .4f))
		{
			SBX = STEP + ((S->ball_x - S->x) / 10);
			SBY = STEP + ((S->ball_x - S->x) / 20);
		}
		else if (S->ball_x <= S->x && S->ball_x >= (S->x - .2f))
		{
			SBX = -STEP - ((S->x - S->ball_x) / 10);
			SBY = STEP + ((S->x - S->ball_x) / 20);
		}
		else
			S->game_state = GAME_FAILED;
	}
}

void			draw_ball(float radius)
{
	const float	deg2_rad = 3.14159 / 180;
	int			i;
	float		degin_rad;

	glTranslatef(S->ball_x, S->ball_y, 0.f);
	glBegin(GL_LINE_LOOP);
	i = -1;
	(void)radius;
	while (++i < 360)
	{
		degin_rad = i * deg2_rad;
		glVertex2f(cos(degin_rad) * radius, sin(degin_rad) * radius);
	}
	glEnd();
}

void			draw_palette(void)
{
	glTranslatef(S->x, -0.9f, 0.f);
	glBegin(GL_QUADS);
	glVertex3f(0.2f, 0.1f, 0.f);
	glVertex3f(-0.2f, 0.1f, 0.f);
	glVertex3f(-0.2f, -0.f, 0.f);
	glVertex3f(0.2f, -0.f, 0.f);
	glEnd();
}

void			draw_case(float x, float y, int value)
{
	glLoadIdentity();
	glTranslatef(x, y, 0.f);
	if (value == 1)
		glColor3f(1.0f, 0.f, 0.f);
	else if (value == 2)
		glColor3f(0.f, 1.f, 0.f);
	else if (value == 3)
		glColor3f(0.f, 0.f, 1.f);
	else if (value == 9)
		glColor3f(1.f, 1.f, 1.f);
	glBegin(GL_QUADS);
	glVertex3f(0.05f, 0.05f, 0.f);
	glVertex3f(-0.05f, 0.05f, 0.f);
	glVertex3f(-0.05f, -0.05f, 0.f);
	glVertex3f(0.05f, -0.05f, 0.f);
	glEnd();
	glColor3f(1.0f, 1.0f, 1.0f);
	glPushMatrix();
}
