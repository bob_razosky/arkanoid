/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 21:16:56 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/03 21:17:43 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"

static char			*get_line(char *str)
{
	char			*line;
	unsigned int	i;

	i = 0;
	if (str == NULL)
		return (NULL);
	while (str[i] != '\0' && str[i] != '\n')
		i++;
	line = ft_strnew(i);
	ft_strncpy(line, str, i);
	line[i] = '\0';
	return (line);
}

static char			*get_over_buff(char *str)
{
	char			*overbuff;
	unsigned int	i;

	i = 0;
	if (str == NULL)
		return (NULL);
	while (str[i] != '\0' && str[i] != '\n')
		i++;
	overbuff = ft_strdup(str + i + 1);
	ft_strdel(&str);
	return (overbuff);
}

static int			free_save(char **save)
{
	ft_strdel(save);
	return (0);
}

int					get_next_line(int const fd, char **line)
{
	char			*buff;
	static char		*save;
	char			*tmp;
	int				ret;

	buff = ft_strnew(BUFF_SIZE + 1);
	if (buff == NULL || line == NULL)
		return (-1);
	ret = 42;
	save = (save == NULL) ? ft_strnew(1) : save;
	while ((ft_strchr(save, '\n') == NULL) && ret > 0)
	{
		if ((ret = read(fd, buff, BUFF_SIZE)) == -1)
			return (-1);
		buff[ret] = '\0';
		tmp = save;
		save = ft_strjoin(save, buff);
		ft_strdel(&tmp);
	}
	ft_strdel(&buff);
	*line = get_line(save);
	if (ret == 0 && *line[0] == '\0')
		return (free_save(&save));
	save = get_over_buff(save);
	return (1);
}
