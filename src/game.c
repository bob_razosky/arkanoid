/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 19:13:29 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/03 21:24:40 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arka.h"
#include "get_next_line.h"

static int			open_next_level(void)
{
	char			*level;
	char			*format;
	int				fd;

	level = ft_itoa(S->level);
	if (S->level > MAX_LEVELS)
	{
		S->game_state = GAME_WON_EVERYTHING;
		game_routine();
	}
	format = ft_strjoin("level/level", level);
	fd = open(format, O_RDONLY);
	if (fd == -1)
	{
		ft_putendl_fd("Bad shit happened !", 2);
		exit (EXIT_FAILURE);
	}
	else
		return (free(level), free(format), fd);
}

void				init(void)
{
	int				fd;
	char			*s;
	int				i;

	i = 0;
	S->step_ball_x = STEP;
	S->step_ball_y = STEP;
	S->ball_x = 0;
	S->ball_y = 0;
	S->level++;
	if (S->game_state == GAME_START)
	{
		fd = open_next_level();
		while (i <= SIZE)
		{
			get_next_line(fd, &s);
			fill_map(S, s, i);
			i++;
		}
	}
	S->game_state = GAME_RUNNING;
}

t_pos				*pos_add(t_pos *next, int y, int x)
{
	t_pos			*pos;

	pos = malloc(sizeof(t_pos));
	pos->mapx = x;
	pos->mapy = y;
	pos->x1 = (0.85 - (0.2 * x));
	pos->x2 = (0.95 - (0.2 * x));
	pos->y1 = (0.85 - (0.2 * y));
	pos->y2 = (0.95 - (0.2 * y));
	pos->next = next;
	return (pos);
}

void				game_routine(void)
{
	if (S->game_state == GAME_FAILED)
	{
		if (--S->life > 0)
			init();
		else
		{
			ft_score(ft_itoa(S->score));
			glfwDestroyWindow(S->win);
			glfwTerminate();
			exit (EXIT_FAILURE);
		}
	}
	else if (S->game_state == GAME_WON)
	{
		ft_putendl_fd("Next level", 2);
		S->game_state = GAME_START;
		init();
	}
	else if (S->game_state == GAME_WON_EVERYTHING)
	{
		ft_putendl("Congrats you've won everything !");
		exit (EXIT_SUCCESS);
	}
}
