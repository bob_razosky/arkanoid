/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 21:19:01 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/08 03:51:17 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arka.h"
#include "get_next_line.h"

t_arka				*singleton(void)
{
	static t_arka	arka;

	return (&arka);
}

static void			update_score(GLFWwindow *window)
{
	char			*score;
	char			*format;

	score = ft_itoa(S->score);
	format = ft_strjoin("Arkanoid. Score:", score);
	glfwSetWindowTitle(window, format);
	free(score);
	free(format);
	glfwPollEvents();
}

int					main(int ac, char **av)
{
	GLFWwindow		*window;
	t_pos			*pos;
	float			y;

	if (!glfwInit())
		exit(EXIT_FAILURE);
	ft_player(av[1], ac);
	S->life = 3;
	init();
	window = glfwCreateWindow(920, 680, "Simple example", NULL, NULL);
	S->win = window;
	test2(window);
	while (!glfwWindowShouldClose(window))
	{
		y = 0;
		test3(window, pos);
		pos = NULL;
		pos = test4(y, pos);
		glfwSwapBuffers(window);
		update_score(window);
		game_routine();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
