/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_stuff.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 20:59:29 by rdantzer          #+#    #+#             */
/*   Updated: 2015/05/03 21:21:04 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arka.h"

int					verif_map(t_arka *arka)
{
	int				y;
	int				x;

	y = 0;
	x = 0;
	while (y <= SIZE)
	{
		x = 0;
		while (x <= SIZE)
		{
			if (arka->map[y][x] >= 1 && arka->map[y][x] < 9)
				return (1);
			x++;
		}
		y++;
	}
	return (0);
}

void				fill_map(t_arka *arka, char *s, int i)
{
	int				j;

	j = 0;
	while (*s)
	{
		arka->map[i][j] = *s - 48;
		j++;
		s++;
	}
}
