/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdantzer <rdantzer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 19:10:22 by jvolonda          #+#    #+#             */
/*   Updated: 2015/05/03 21:23:11 by rdantzer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arka.h"
#include <fcntl.h>

void			ft_player(char *player, int ac)
{
	int			fd;

	if (ac != 2)
	{
		ft_putstr("I need a pseudo to work\n");
		exit(EXIT_FAILURE);
	}
	fd = open("scores", O_RDWR | O_CREAT | O_APPEND, 0666);
	write(fd, "player ", 7);
	write(fd, player, ft_strlen(player));
	write(fd, " ", 1);
}

void			ft_score(char *scores)
{
	int			fd;
	int			ret;
	char		buffer[512];

	fd = open("scores", O_RDWR | O_APPEND);
	write(fd, "has scored ", 11);
	write(fd, scores, ft_strlen(scores));
	write(fd, "\n", 1);
	free(scores);
	close(fd);
	open("scores", O_RDONLY);
	ft_putendl("Game Over\n\033[31m<<<SCORES>>>\033[0m");
	while ((ret = read(fd, buffer, 512)))
		write(1, buffer, ret);
	close(fd);
}
