#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aastruc <aastruc@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/02 23:34:18 by aastruc           #+#    #+#              #
#    Updated: 2015/05/02 23:39:08 by aastruc          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

.PHONY:				all, libft, libglfw, clean, fclean, re

CC =				clang

CFLAG =				-Wall -Werror -Wextra

NAME =				arkanoid

INC_PATH =			./inc/ -I ./libft/includes

SRC_PATH =			./src/

OBJ_PATH =			./obj/

GLFW_PATH =			./glfw/

LIBFT_PATH =		./libft/

LIBGLFW_ROOT_PATH =	./libglfw/

LIBGLFW_PATH =		./libglfw/lib/

INC_LIBFT_PATH =	./libft/inc/

INC_LIBGLFW_PATH =	./libglfw/include/GLFW/

SRC_NAME =			main.c 		draw_stuff.c 		map_stuff.c 	misc.c\
					game.c		player.c			get_next_line.c


OBJ_NAME =			$(SRC_NAME:.c=.o)

LIBFT_NAME =		libft.a

LIBGLFW_NAME =		libglfw3.a

LIBFT_FLAG =		-L$(LIBFT_PATH) -lft

LIBGLFW_FLAG =		-L$(LIBGLFW_PATH) -lglfw3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework GLUT

SRC =				$(addprefix $(SRC_PATH),$(SRC_NAME))

OBJ =				$(addprefix $(OBJ_PATH),$(OBJ_NAME))

LIBFT =				$(addprefix $(LIBFT_PATH),$(LIBFT_NAME))

LIBGLFW =			$(addprefix $(LIBGLFW_PATH),$(LIBGLFW_NAME))

all:				libft libglfw $(NAME)

$(NAME):			$(OBJ)
					$(CC) $(LIBFT_FLAG) $(LIBGLFW_FLAG) -o $@ $^

$(OBJ_PATH)%.o:		$(SRC_PATH)%.c
					@mkdir -p $(OBJ_PATH)
					$(CC) $(CFLAG) -I$(INC_PATH) -I$(INC_LIBFT_PATH) -I$(INC_LIBGLFW_PATH) -o $@ -c $<

libft:				$(LIBFT)

$(LIBFT):
					git submodule add -f https://github.com/Raphy42/Libft.git
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					echo "BONJOUR JE MAPPELLE MR BANANE ET JE SUIS UN BON CHYBRE"
					make -C $(LIBFT_PATH)

libglfw:			$(LIBGLFW)

$(LIBGLFW):
					git submodule add -f https://github.com/glfw/glfw.git
					@mkdir -p $(LIBGLFW_ROOT_PATH)
					cd $(GLFW_PATH) && cmake -DCMAKE_INSTALL_PREFIX=../$(LIBGLFW_ROOT_PATH) .
					make -C $(GLFW_PATH)
					make install -C $(GLFW_PATH)

clean:
					rm -rfv $(OBJ)

fclean:				clean
					rm -rfv $(LIBGLFW_ROOT_PATH)
					rm -rfv $(NAME)
					rm -rf .git/modules/ .git/index .gitmodules $(GLFW_PATH) $(LIBFT_PATH)

re: 				fclean all
